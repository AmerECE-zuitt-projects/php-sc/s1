<?php require_once "./code.php"?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activity</title>
</head>
<body>

    <h2>Full Address</h2>
    <p><?php echo getFullAddress("Philippines", "Metro Manila", "Quezon City", "3F Caswyn Bldg,. Timog Avenue") ?></p>
    <p><?php echo getFullAddress("Philippines", "Metro Manila", "Makati City", "3F Enzo Bldg,. Buendia Avenue") ?></p>

    <h2>Letter-Based Grading</h2>
    <p><?php echo getLetterGrade(87) ?></p>
    <p><?php echo getLetterGrade(94) ?></p>
    <p><?php echo getLetterGrade(74) ?></p>

</body>
</html>